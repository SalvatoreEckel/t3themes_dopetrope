<?php
defined('TYPO3_MODE') || die();

$extensionKey = 't3themes_dopetrope';

/***************
 * Register PageTS
 */

// BackendLayouts
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TSconfig/Mod/WebLayout/BackendLayouts.txt',
    'T3themes DopeTrope - Backend Layouts'
);

// BackendLayouts
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TSconfig/RTE.typoscript',
    'T3themes DopeTrope - CKEditor Configuration'
);
