<INCLUDE_TYPOSCRIPT: source="DIR:EXT:t3themes_dopetrope/Configuration/TypoScript/Setup/" extensions="ts,txt,typoscript">

#### PAGE
page = PAGE
page {
    typeNum = 0
    #shortcutIcon = {$website.shortcutIcon}
    10 = FLUIDTEMPLATE
    10 {
        templateName = TEXT
        templateName.stdWrap.cObject = CASE
        templateName.stdWrap.cObject {
            key.data = pagelayout

            pagets__dopetropeclean = TEXT
            pagets__dopetropeclean.value = Default

            default = TEXT
            default.value = Default
        }

        templateRootPaths.0 = EXT:t3themes_dopetrope/Resources/Private/Templates/Page/
        partialRootPaths.0 = EXT:t3themes_dopetrope/Resources/Private/Partials/Page/
        layoutRootPaths.0 = EXT:t3themes_dopetrope/Resources/Private/Layouts/Page/

        ### DATA PREPROCESSING
        dataProcessing {
            10 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            10 {
                levels = 3
                includeSpacer = 1
                as = mainnavigation
            }
            20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
            20 {
                entryLevel = 1
                levels = 2
                expandAll = 0
                includeSpacer = 1
                as = subnavigation
            }
        }

        ### VARIABLES
        variables {
            pageTitle = TEXT
            pageTitle.data = page:title
            rootPage = TEXT
            rootPage.data = leveluid:0
        }
    }
    meta {
        #viewport = width=device-width, initial-scale=1
        keywords.data = DB:pages:1:keywords 
        keywords.override.field = keywords 
        description.data = DB:pages:1:description 
        description.override.field = description 
        abstract.data = DB:pages:1:abstract 
        abstract.override.field = abstract 
        author.data = DB:pages:1:author 
        author.override.field = author
        web_author.data = DB:pages:1:author 
        web_author.override.field = author
    }

    #includeCSSLibs {}
    includeCSS {
        theme_dopetrope = EXT:t3themes_dopetrope/Resources/Public/css/main.css
    }
    #includeJSLibs {}
    includeJSFooterlibs {
        jquery = EXT:t3themes_dopetrope/Resources/Public/js/jquery.min.js
        jquery.forceOnTop = 1
        jquerydropotron_js = EXT:t3themes_dopetrope/Resources/Public/js/jquery.dropotron.min.js
        skel_js = EXT:t3themes_dopetrope/Resources/Public/js/skel.min.js
        skelviewport_js = EXT:t3themes_dopetrope/Resources/Public/js/skel-viewport.min.js
        util_js = EXT:t3themes_dopetrope/Resources/Public/js/util.js
        main_js = EXT:t3themes_dopetrope/Resources/Public/js/main.js
    }
    #jsFooterInline {}
    headerData {
        ### EXAMPLE: Loading Google Fonts
		#12 = TEXT
		#12.value = <link href='//fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
    	13 = TEXT
    	13.value (
<!--[if lte IE 8]><script src="js/ie/html5shiv.js"></script><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
<!--[if lte IE 8]><script src="js/ie/respond.min.js"></script><![endif]-->
)
    }
}

[userFunc = TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('t3cms')]
page.10.dataProcessing.30 = SalvatoreEckel\T3cms\DataProcessing\T3themesConfProcessor
page.10.dataProcessing.30.fieldName = t3themes_conf
page.10.dataProcessing.30.as = t3themesConf
page.10.dataProcessing.30.rootpageId = TEXT
page.10.dataProcessing.30.rootpageId {
    insertData = 1
    data = leveluid : 0
}
[global]

# Adding configuration to prevent throwing errors when required extensions are not loaded
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:t3themes_dopetrope/Configuration/TypoScript/Fallbacks/" extensions="ts,txt,typoscript">
