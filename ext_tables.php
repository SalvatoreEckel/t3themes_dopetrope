<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
    {

    	# Backend Module 1
        if (TYPO3_MODE === 'BE' && !(TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_INSTALL)) {
		    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		        'SalvatoreEckel.' . $extKey,
				'help',
				't3themesdopetrope',	// Name of the module
				'after:aboutmodules',
		        array(
		            'Dopetrope' => 'contentElements,liveDemo',
		        ),
		        array(
					'access'    => 'user,group',
					'icon'      => 'EXT:' . $extKey . '/ext_icon.png',
		            'labels' => 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang.xlf',
		        )
		    );
		}

    },
    $_EXTKEY
);
