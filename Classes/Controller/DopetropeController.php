<?php
namespace SalvatoreEckel\T3themesDopetrope\Controller;

#use \TYPO3\CMS\Core\Utility\GeneralUtility;
#use \TYPO3\CMS\Core\Messaging\AbstractMessage;
#use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * DopetropeController
 */
class DopetropeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * action contentElements
     *
     * @return void
     */
    public function contentElementsAction()
    {

    }

    /**
     * action liveDemo
     *
     * @return void
     */
    public function liveDemoAction()
    {
        $this->view->assign('extkey', 't3themes_dopetrope');
    }

}
