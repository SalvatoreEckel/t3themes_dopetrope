# EXT:t3themes_dopetrope #

This is a ready to use template for TYPO3. Together with EXT:t3cms you have an excellent website now!

### Abandoned extension ###

This extension has no maintainer at the moment. If you like to get the maintainer of this extension, fill out the TYPO3.org registration form: https://extensions.typo3.org/faq/get-maintainer/

### General Information ###

* t3themes_dopetrope
* v2.4.1

### How do I get set up? ###

* Install the extension from TER
* Make sure EXT:t3cms is installed
* Include static templates and pageTS from t3cms & t3themes_dopetrope

* Have fun with your page settings and backend layouts. Check out the backend module.

### Who do I talk to? ###

* Salvatore Eckel
* salvaracer@gmx.de